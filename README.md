The core backend code for the CiteNet/MOVE: https://github.com/move-tool/gephi-plugins


Several dependencies need to be installed in a local maven repository; see `pom.xml` for details.