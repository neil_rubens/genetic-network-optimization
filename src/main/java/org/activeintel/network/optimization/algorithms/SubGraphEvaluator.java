/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.lang.Float;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;
/*
import org.gephi.data.attributes.api.AttributeController;
import org.gephi.data.attributes.api.AttributeRow;
import org.gephi.data.attributes.api.AttributeValue;
import org.gephi.datalab.api.AttributeColumnsController;
import org.gephi.graph.api.Attributes;
*/
import org.gephi.graph.api.Column;
import org.gephi.graph.api.ColumnIterable;
import org.gephi.graph.api.Node;
import org.gephi.statistics.spi.Statistics;
import org.gephi.statistics.spi.StatisticsBuilder;
import org.gephi.statistics.spi.StatisticsUI;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultDirectedWeightedGraph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.openide.util.Lookup;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

/**
 * Calculates the fitness score of a subGraph in relation to superGraph
 * and with regards to various utility values
 * 
 * mixes in model and controller for now
 * to use ui see org.activeintel.network.optimization.algorithms.ui
 * 
 * @author Neil Rubens
 */
public class SubGraphEvaluator implements FitnessEvaluator<Graph> {
    
    private static final Logger log = Logger.getLogger(SubGraphEvaluator.class.getName());            
    
    private Hashtable<String, SubGraphEvaluationCriterion> evalCriteria; // currently stores just the metric one; REFACTOR: include the criteria from bellow if possible as well; although some of the criteria is calculated for the subgraphs on the fly    
    private Float subGraphInDegreeUtility = new Float(1.0);
    private Float subGraphOutDegreeUtility = new Float(1.0);
    private Float graphInDegreeUtility = new Float(1.0);
    private Float graphOutDegreeUtility = new Float(1.0);
    private Float edgeWeightFactorUtility = new Float(1.0);
    private Float nodeUtility = new Float(-1.0);;    
    private Graph superGraph;
    
    
    /**
     * Typically negative values are not allowed by optimization algorithms.
     * isNormalized allows to indicate if normalization is needed.
     * 
     * @see calculateAdjustment, getFitness
     */
    private Boolean isNormalized = true;
    
    
    private Float minValue = new Float(1.0);
    private Float adjustmentValue = (float) 0.0;
    
    
    public String toString(){
        StringBuffer strb = new StringBuffer();
        strb.append("SubGraphEvaluator: \n");
        strb.append("subGraphInDegreeUtility: " + subGraphInDegreeUtility + "\n");                
        strb.append("subGraphOutDegreeUtility: " + subGraphOutDegreeUtility + "\n");                        
        strb.append("graphInDegreeUtility: " + graphInDegreeUtility + "\n");                        
        strb.append("graphOutDegreeUtility: " + graphOutDegreeUtility + "\n");                        
        strb.append("edgeWeightFactorUtility: " + edgeWeightFactorUtility + "\n");                        
        strb.append("nodeUtility: " + nodeUtility + "\n");        
        strb.append("isNormalized: " + isNormalized + "\n");                
        
        return strb.toString();
    }
    

    public SubGraphEvaluator(){
        super();        
        // Add metrics
        // Add metrics UI's in the corresponding UI class
        this.evalCriteria = new Hashtable<String, SubGraphEvaluationCriterion>();
    }

    
    
    
    /**
     * Typically negative values are not allowed by optimization algorithms
     * find the min value 
     * where min <- <minValue>
     * actually it might be smaller or bigger depending on weightFactor
     * am not doing it right now since would need to find maxWeight
     * 
     * values are adjusted only if they are negative
     * in particular have to be aware of not adjusting 0 (since this could introduce undesirable behavior)
     * @see adjust
     */
    
    public Float calculateAdjustmentValue(){
        Float adjustment = (float) 0.0;
        // find the smallest value
        Float min = Collections.min(Arrays.asList(new Float[]{subGraphInDegreeUtility, subGraphOutDegreeUtility, graphInDegreeUtility, graphOutDegreeUtility}));

        // adjust if needed
        if (min < minValue)
        {
            adjustment = minValue - min;
        }
        
        return adjustment;
    }
    
    
    
    @Override
    public double getFitness(Graph candidate, List<? extends Graph> population) {
        return getFitness(candidate);
    }
    
    
    /**
     * returns fitness for each of the nodes
     * @param nodes
     * @return 
     */
    public Map<Object,Double> getFitness(Graph candidate, Collection nodes){                
        
        Map<Object,Double> nodesFitness = new HashMap<Object,Double>();
                
        // calculate adjustment for normalization if needed
        if (isNormalized){
            this.setAdjustmentValue(this.calculateAdjustmentValue());
        }else{ // don't need adjustment; <- 0
            this.setAdjustmentValue((float)0);            
        }
                        
        // Convert to the most restrictive type (may throw errors: so need to add reflection etc.)
        DirectedWeightedSubgraph subGraph = (DirectedWeightedSubgraph) candidate;    
        
        // Cast supergraph
        DefaultDirectedWeightedGraph superGraph = (DefaultDirectedWeightedGraph) this.superGraph;        
        
        log.fine("Metrics Criteria: " + evalCriteria.toString() );

        // Get fitness of each node
        for ( Object node : nodes ){
            double fitness = 0;
            // In/Out-Degree of Sub/Supger-graph
            // values of supergraph should not overlap with subgraphs so use (valSuperGraph - valSubGraph)
            // e.g. in-degree of node in subgraph and supergraph; 

            
            // get & add metrics values from node's attributes (gephi)
            Node gNode = (Node) node;
            Column[] atrColumns = gNode.getAttributeColumns().toArray();
            for (Column atr : atrColumns){
                String criterionName = atr.getTitle();
                 SubGraphEvaluationCriterion criterion = evalCriteria.get(criterionName);
                 if ( criterion != null ){
                     Float criterionWeight = criterion.getWeight();
                     Float criterionValue = Float.valueOf( gNode.getAttribute(atr).toString() );
                     Float criterionFitness = criterionWeight * criterionValue;
                     log.finest(criterionName + " : " + " val: " + criterionValue + 
                             " weight: " + criterionWeight + 
                             " fitness: " + criterionFitness);
                     fitness += criterionFitness;
                 }                 
            }
                
            
            // in-degree sub/super-graph
            //System.out.println("node: " + node);
            int inDegreeSubgraph = subGraph.inDegreeOf(node);
            int inDegreeSupergraph = superGraph.inDegreeOf(node);
            fitness += inDegreeSubgraph * getSubGraphInDegreeUtility();
            fitness += (inDegreeSupergraph - inDegreeSubgraph) * this.getGraphInDegreeUtility();                        
            
            // out-degree sub/super-graph            
            int outDegreeSubgraph = subGraph.outDegreeOf(node);
            int outDegreeSupergraph = superGraph.outDegreeOf(node);
            fitness += outDegreeSubgraph * getSubGraphOutDegreeUtility();
            fitness += (outDegreeSupergraph - outDegreeSubgraph) * this.getGraphOutDegreeUtility();
            
            // add utility of the node itself
            fitness += this.getNodeUtility();
        
            // Add it to the list
            nodesFitness.put(node, fitness);
        }
        
        return nodesFitness;
    }
    
    
    public double getFitness(Graph candidate) {  
        if (candidate == null){
            return 0;
        }
        
        double fitness = 0;
        Set nodes = candidate.vertexSet();
        Map<Object, Double> nodesFitness = getFitness(candidate, nodes);
        
        // currently fitness is the sum of the node's fitness
        for (Object node: nodes){
            fitness += nodesFitness.get(node);
        }
        
        
        return fitness;
    }
    
    
    
    

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isNatural() {
        return true;
    }

    

    /**
     * @return the superGraph
     */
    public Graph getSuperGraph() {
        return superGraph;
    }

    /**
     * @param superGraph the superGraph to set
     */
    public void setSuperGraph(Graph superGraph) {
        this.superGraph = superGraph;
    }

    /**
     * @return the subGraphInDegreeUtility
     */
    public Float getSubGraphInDegreeUtility() {
        return adjust(subGraphInDegreeUtility);
    }

    /**
     * @param subGraphInDegreeUtility the subGraphInDegreeUtility to set
     */
    public void setSubGraphInDegreeUtility(Float subGraphInDegreeUtility) {
        this.subGraphInDegreeUtility = subGraphInDegreeUtility;
    }

    /**
     * @return the subGraphOutDegreeUtility
     */
    public Float getSubGraphOutDegreeUtility() {
        return adjust(subGraphOutDegreeUtility);
    }

    /**
     * @param subGraphOutDegreeUtility the subGraphOutDegreeUtility to set
     */
    public void setSubGraphOutDegreeUtility(Float subGraphOutDegreeUtility) {
        this.subGraphOutDegreeUtility = subGraphOutDegreeUtility;
    }

    /**
     * @return the graphInDegreeUtility
     */
    public Float getGraphInDegreeUtility() {
        return adjust(graphInDegreeUtility);
    }
    
    
    /**
     * Only adjusts values that are negative
     * (in particular should not adjust 0; since would introduce strange behavior)
     * 
     * @param val
     * @return 
     */
    public Float adjust(float val){
        // doesn't need to be adjusted (since is not normalized)
        if (!this.isNormalized)
        {
            return val;
        }
        // adjust
        else{
            // doesn't need to be adjusted
            if (val>=0){
                return val;
            }
            else{
                return val + this.getAdjustmentValue();
            }            
        }
    }
    

    /**
     * @param graphInDegreeUtility the graphInDegreeUtility to set
     */
    public void setGraphInDegreeUtility(Float graphInDegreeUtility) {
        this.graphInDegreeUtility = graphInDegreeUtility;
    }

    /**
     * @return the graphOutDegreeUtility
     */
    public Float getGraphOutDegreeUtility() {
        return adjust(graphOutDegreeUtility);
    }

    /**
     * @param graphOutDegreeUtility the graphOutDegreeUtility to set
     */
    public void setGraphOutDegreeUtility(Float graphOutDegreeUtility) {
        this.graphOutDegreeUtility = graphOutDegreeUtility;
    }

    /**
     * @return the edgeWeightFactorUtility
     */
    public Float getEdgeWeightFactorUtility() {
        return adjust(edgeWeightFactorUtility);
    }

    /**
     * @param edgeWeightFactorUtility the edgeWeightFactorUtility to set
     */
    public void setEdgeWeightFactorUtility(Float edgeWeightFactorUtility) {
        this.edgeWeightFactorUtility = edgeWeightFactorUtility;
    }

    
    /**
     * @return the isNormalized
     */
    public Boolean getIsNormalized() {
        return isNormalized;
    }

    /**
     * @param isNormalized the isNormalized to set
     */
    public void setIsNormalized(Boolean isNormalized) {
        log.fine("isNormalized: " + this.isNormalized + " -> " + isNormalized);        
        this.isNormalized = isNormalized;
    }

    /**
     * @return the adjustmentValue
     */
    public Float getAdjustmentValue() {
        return adjustmentValue;
    }

    /**
     * @param adjustmentValue the adjustmentValue to set
     */
     public void setAdjustmentValue(Float adjustmentValue) {
        this.adjustmentValue = adjustmentValue;
    }

    /**
     * @return the nodeUtility
     */
    public Float getNodeUtility() {
        return adjust(nodeUtility);
    }

    /**
     * @param nodeUtility the nodeUtility to set
     */
    public void setNodeUtility(Float nodeUtility) {
        this.nodeUtility = nodeUtility;
    }



    
    
    /**
     * feed criteria that is used for evaluating fitness
     * currently it is just the fields associated with nodes
     * REFACTOR: may want to add other criteria e.g. in/out-degree; etc.
     * 
     */    
    public void initEvaluationCriteria() {
        System.out.println("initEvaluationCriteria");
        Graph graph = getSuperGraph();
        Node node = (Node) graph.vertexSet().iterator().next(); // grab the first node
        // get metrics names from node (gephi)        
        Column[] atrs = node.getAttributeColumns().toArray();
        //for (int i = 0; i < atrs.countValues(); i++) {
        for (Column atr : atrs) {
            System.out.println("atr: " + atr);
            SubGraphEvaluationCriterion criterion = new SubGraphEvaluationCriterion(atr.getTitle());
            if (evalCriteria.get(criterion.getName()) == null ){ // don't over-write if one exists already                
                // ignore id & label columns, and other non metric columns
                Set<String> nonMetricCols = new HashSet<String>(Arrays.asList("Id", "Label", "subgraph_fitness", "subgraph_exclude", "Interval"));
                if (! nonMetricCols.contains(criterion.getName())){
                    evalCriteria.put(criterion.getName(), criterion); 
                }
            }
        }                
        
        // Normalize
        normalizeMetrics(graph.vertexSet());
    }
    
    
    /**
     * Returns criteria that is used for evaluating fitness
     * currently it is just the fields associated with nodes
     * REFACTOR: may want to add other criteria e.g. in/out-degree; etc.
     * 
     */
    public Hashtable<String, SubGraphEvaluationCriterion> getEvaluationCriteria() {
        initEvaluationCriteria();
        return this.evalCriteria;
    }

    
    /**
     * Normalizes metrics in evalCriteria [0,1]
     * and updates them in "data table" of gephi
     * 
     */
    private void normalizeMetrics(Set nodes) {
        System.out.println("normalizeMetrics");
        Node node = (Node) nodes.iterator().next();
        Column[] atrs = node.getAttributeColumns().toArray();
        //for (int i = 0; i < atrs.countValues(); i++) {
        for (Column atr: atrs) {
            System.out.println("atr: " + atr);
            String atrName = atr.getTitle();
            SubGraphEvaluationCriterion criterion = evalCriteria.get(atrName);
            System.out.println("criterion: " + criterion);
            log.fine("criterion: " + criterion);
            
            if (criterion !=null){ // is it a metric
                // ? needs normalization
                if (criterion.getIsNormalized()){
                    log.finer(criterion.toString());
                    normalize(atr, nodes);
                }                
            }
        }
    }

    /**
     * Normalizes metric to [0,1]
     * and updates it in "data table" of gephi
     */
    private void normalize(Column atr, Set nodes) {
        log.finer(atr.toString());

        // get min-max stats
        Set vals = new HashSet();
        for (Node node : (Set<Node>) nodes){
            vals.add(node.getAttribute(atr));
        }
        Object min = Collections.min(vals);
        Object max = Collections.max(vals);

        // (double dataHigh, double dataLow, double normalizedHigh, double normalizedLow) {
        NormUtil norm = new NormUtil(max, min, 1.0, 0.0);

        // itterate through data and normalize it (w/in table)
        for (Node node : (Set<Node>) nodes){
            Object val = node.getAttribute(atr);
            node.setAttribute(atr, norm.normalize(val)); // set value
        }
    }


    
    
    
    
}
