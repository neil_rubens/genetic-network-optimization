/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.beans.*;
import java.io.Serializable;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.jgrapht.Graph;
import org.jgrapht.graph.Subgraph;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;
import java.beans.*;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.logging.Logger;
import org.gephi.graph.api.Node;
import org.jgrapht.DirectedGraph;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DirectedSubgraph;
import org.jgrapht.graph.DirectedWeightedSubgraph;

/**
 * Could be used as a JavaBean
 * 
 * @author Neil Rubens
 */
public class SubGraphFactory extends AbstractCandidateFactory<Graph> implements Serializable, CandidateFactory<Graph> {

    
    
    private static final Logger log = Logger.getLogger(SubGraphFactory.class.getName());        
    
    private Integer numSubGraphNodes;
    private Graph superGraph; // contains all of the nodes
    private Collection subgraphCandidateNodes; // contains nodes for subgraph generation: supergraph - exclusionGraph;  @see setExcludedNodes
    
    
    public SubGraphFactory() {
    }
    
    
    /**
     * Creates SubGraphFactory
     * w/ some of the values initialized
     * 
     * mostly used for tests, etc
     * 
     */
    public static SubGraphFactory createSubGraphFactory(Graph superGraph){
        SubGraphFactory factory = new SubGraphFactory();
        factory.setSuperGraph(superGraph);
        factory.setNumSubGraphNodes(10);
        
        return factory;
    }
    
    
    /**
     * Generates SubGraphs of graphModel with the number of nodes equal to numSubGraphNodes
     * 
     * @param numSubGraphNodes 
     */
    public SubGraphFactory (Graph superGraph, int numSubGraphNodes){

        this.superGraph = superGraph;
        this.subgraphCandidateNodes = new Vector(superGraph.vertexSet());
        this.numSubGraphNodes = numSubGraphNodes;        

        if (numSubGraphNodes > superGraph.vertexSet().size()){
            throw new IllegalArgumentException("numSubGraphNodes should be smaller than numGraphNodes");
        }
    }
    
    
    /**
     * Nodes will be excluded from any of the generated subgraphs;
     * these nodes will be removed from subgraphCandidates
     * @param graph: contains nodes that must be excluded from generated subGraphs
     */
    public void setExcludedNodes(Collection nodeIds){
        this.subgraphCandidateNodes = new Vector(superGraph.vertexSet());
        System.out.println("num of nodes before exclusion: " +  subgraphCandidateNodes.size());                
        Set nodes = superGraph.vertexSet();
        for (Object obj : nodes){
            Node node = (Node) obj;
            int id = Integer.valueOf(node.getId().toString());
            if (nodeIds.contains(id)){
                subgraphCandidateNodes.remove(node);
            }
        }
        System.out.println("num of nodes after exclusion: " +  subgraphCandidateNodes.size());        
        System.out.println(subgraphCandidateNodes);
    }
    
    
    
    @Override
    public Graph generateRandomCandidate(Random rnd) {
        // Select Nodes for the subgraph
        System.out.println("generateRandomCandidate: select nodes");
        Object[] graphNodes = subgraphCandidateNodes.toArray();
        int graphNodesCount = graphNodes.length;
        HashSet subgraphNodes = new HashSet();
        while (subgraphNodes.size() < getNumSubGraphNodes()){
            subgraphNodes.add( graphNodes[ rnd.nextInt(graphNodesCount)] );
        }
                
        
        
        // Construct subraph
        System.out.println("generateRandomCandidate: construct subGraph");
        Subgraph subGraph = new Subgraph(getSuperGraph(), subgraphNodes);
        DirectedWeightedSubgraph weightedSubGraph = new DirectedWeightedSubgraph((WeightedGraph) getSuperGraph(), subgraphNodes, subGraph.edgeSet());
        //DirectedSubgraph weightedSubGraph = new DirectedSubgraph((DirectedGraph)getSuperGraph(), subgraphNodes, subGraph.edgeSet());        
        //Subgraph subGraph = new DirectedWeightedSubgraph((WeightedGraph) getSuperGraph(), subgraphNodes);        
        System.out.println("returning");
        return weightedSubGraph;
    }
    
    

    
    /**
     * create subGraph based on the ids in subgraphNodeIdsArray
     * this method is more reliable since it avoids the problem with equals when the object types are different etc.
     * limitation is that it uses numeric ids
     * 
     * @param subgraphNodeIdsArray
     * @return 
     */
    public Subgraph createSubgraph(Integer[] subgraphNodeIdsArray) {    
        List<Integer> subgraphNodeIds = Arrays.asList(subgraphNodeIdsArray);
        Set graphNodes = this.getSuperGraph().vertexSet();
        HashSet subgraphNodes = new HashSet();
        
        for (Object obj : graphNodes){
            Node node = (Node) obj;
            int id = Integer.valueOf(node.getId().toString());
            //System.out.println("id: " + id);
            if (subgraphNodeIds.contains(id)){
                subgraphNodes.add(obj);
                //System.out.println("adding id: " + id);                
            }
        }
        
        
        // Construct subraph
        Subgraph subGraph = new Subgraph(getSuperGraph(), subgraphNodes);        
        DirectedWeightedSubgraph weightedSubGraph = new DirectedWeightedSubgraph((WeightedGraph) getSuperGraph(), subgraphNodes, subGraph.edgeSet());        
        
        return weightedSubGraph;
    }
    
    
    /**
     * creates subgraph using ids of nodes @see createSubgraph(Integer[] subgraphNodeIdsArray)
     * 
     * 
     * @param nodes
     * @return 
     */
    public Subgraph createSubgraph(Set<Node> nodes){

        ArrayList <Integer> ids = new ArrayList<Integer>();
        
        for (Node node : nodes){
            ids.add(Integer.valueOf(node.getId().toString()));
        }
        
        return createSubgraph(ids.toArray(new Integer[ids.size()]));
        
    }
    
    
    
    

    /**
     * @return the numSubGraphNodes
     */
    public Integer getNumSubGraphNodes() {
        return numSubGraphNodes;
    }

    /**
     * @param numSubGraphNodes the numSubGraphNodes to set
     */
    public void setNumSubGraphNodes(Integer numSubGraphNodes) {
        this.numSubGraphNodes = numSubGraphNodes;
    }

    /**
     * @return the superGraph
     */
    public Graph getSuperGraph() {
        return superGraph;
    }

    /**
     * @param superGraph the superGraph to set
     */
    public void setSuperGraph(Graph superGraph) {
        this.superGraph = superGraph;
        this.subgraphCandidateNodes = new Vector(superGraph.vertexSet());        
    }
    


    /**
     * Creates subgraph of exactly the same class as <subGraph>
     * 
     * 
     * @param superGraph
     * @param subGraph used just for finding the right class constructor
     * @param vertexSet nodes that will constitute subGraph
     * @param edgeSet edges superset; only the edges that connect nodes in subGraph should be returned
     */
    public static Graph createSubgraph(Graph superGraph, Graph subGraph, Set vertexSet, Set edgeSet) {
        Graph newGraph = null;
        try {
            Constructor constructor = null;
            Constructor[] constructors = subGraph.getClass().getConstructors();
            for (Constructor c : constructors) {
                Class[] paramTypes = c.getParameterTypes();
                if (paramTypes[0].isAssignableFrom(superGraph.getClass())) {
                    // This is our constructor!
                     constructor = c;
                     break;
                }
            }
             
            if (constructor == null) throw new Exception("Unable to make hacked graph :(");
             newGraph = (Graph)constructor.newInstance(superGraph, vertexSet, edgeSet);
             
             //System.out.println("passed in edges: " + edgeSet);
             //System.out.println("returned edges: " + newGraph.edgeSet());             
             
             
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }                                         
        
        
        return newGraph;
    }

    
    
}
