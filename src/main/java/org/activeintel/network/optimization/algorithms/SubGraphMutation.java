/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.beans.*;
import java.io.Serializable;


import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
//import org.gephi.graph.api.Edge;
//import org.gephi.graph.api.Node;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.gephi.graph.api.Node;
import org.jgrapht.Graph;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.jgrapht.graph.Subgraph;
import org.uncommons.maths.number.ConstantGenerator;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;


/**
 *
 * @author neil
 */
public class SubGraphMutation implements Serializable, EvolutionaryOperator<Graph> {
    
    private ConstantGenerator<Probability> pMutationGenerator;
    Object[] genePool;
    private Graph superGraph;
    private Float pMutation;
    
    
    
    @Override
    public List<Graph> apply(List<Graph> candidates, Random random) {
        List<Graph> mutatedPopulation = new ArrayList<Graph>(candidates.size());       
        
        for (Graph g : candidates){
            mutatedPopulation.add(mutate(g, random));            
        }
        
        return mutatedPopulation;        
    }
    

    public SubGraphMutation()
    {        
    }
    
    
    public static SubGraphMutation createSubGraphMutation(Graph superGraph){
        SubGraphMutation mutation = new SubGraphMutation();
        mutation.setSuperGraph(superGraph);
        mutation.setpMutation( (float) 0.1);
        
        return mutation;
    }
    
    
    /**
     * Specified nodes will be removed from the genePool
     * 
     * HACK: introduces dependency on gephi.Node; should be refactored
     * 
     * @param nodes 
     */
    public void excludeNodes(Collection ids){
        System.out.println("GenePool: " + genePool.length);        
        if (ids == null){
            return;
        }
        // remove node w/ excluded ids from genePool
        else{
            Vector vGene = new Vector();
            for (Object obj : genePool){
                Node node = (Node) obj;
                int id = Integer.valueOf(node.getId().toString());
                if (! ids.contains(id)){
                    vGene.add(obj);
                }
            }
            genePool = vGene.toArray();
            System.out.println("GenePool: " + genePool.length);
        }
    }    
    
    
    /**
     * Creates a mutation operator that is applied with the given
     * probability and draws its characters from the specified alphabet.
     * @param alphabet The permitted values for each character in a string.
     * @param mutationProbability The probability that a given character
     * is changed.
     */
    public SubGraphMutation(Graph superGraph, Float pMutation)
    {
        // LATER: Refactor out?
        this.setSuperGraph(superGraph);
        this.setpMutation(pMutation);
    }

    private Graph mutate(Graph g, Random random)  {                  
        //System.out.println("before mutation: " + g);
        
        // Need to make an object of the same actual class as g
        Graph mutatedGraph = SubGraphFactory.createSubgraph(superGraph, g, g.vertexSet(), g.edgeSet());
        
        //System.out.println("before mutation (recast): " + g);        
        
        mutatedGraph.removeAllVertices(g.vertexSet()); // remove all nodes; will be re-added/mutated later        
        //System.out.println("before mutation (after edge removal): " + superGraph);                
                        
        //Subgraph mutatedGraph = new Subgraph(getSuperGraph(), new HashSet() );
        Object[] nodes =  g.vertexSet().toArray();
        
        for (int i = 0; i < nodes.length; i++){
            // add mutated node
            if (pMutationGenerator.nextValue().nextEvent(random)){                
                Object node = genePool[random.nextInt(genePool.length)];                                
                mutatedGraph.addVertex(node);
            }
            // add original node            
            else
            {
                mutatedGraph.addVertex(nodes[i]);                
            }
        }
        
        // Due to the dups the size of the graph might be too small; adjust it accordingly (results in a slightly higher mutation rate)
        while (mutatedGraph.vertexSet().size() < g.vertexSet().size()){
                mutatedGraph.addVertex(genePool[random.nextInt(genePool.length)]);            
        }      
        
        // Add edges
        mutatedGraph = SubGraphFactory.createSubgraph(superGraph, mutatedGraph, mutatedGraph.vertexSet(), superGraph.edgeSet());
        
        //System.out.println("after mutation: " + mutatedGraph);     
        
        //System.out.println("--------------");        

        return mutatedGraph;
    }

    /**
     * @return the pMutation
     */
    public Float getpMutation() {
        return pMutation;
    }

    /**
     * @param pMutation the pMutation to set
     */
    public void setpMutation(Float pMutation) {
        this.pMutation = pMutation;
        this.pMutationGenerator = new ConstantGenerator<Probability>(new Probability(pMutation));        
    }

    /**
     * @return the superGraph
     */
    public Graph getSuperGraph() {
        return superGraph;
    }

    /**
     * @param superGraph the superGraph to set
     */
    public void setSuperGraph(Graph superGraph) {
        this.superGraph = superGraph;
        this.genePool = superGraph.vertexSet().toArray();        
    }
    
    
}
