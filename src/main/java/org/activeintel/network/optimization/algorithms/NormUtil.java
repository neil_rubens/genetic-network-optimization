/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.util.logging.Logger;

/**
 *
 * @author http://www.heatonresearch.com/wiki/Range_Normalization
 */
public class NormUtil {
 
        private static final Logger log = Logger.getLogger(NormUtil.class.getName());                
	private double dataHigh;
	private double dataLow;
	private double normalizedHigh;
	private double normalizedLow;
 
	/**
	 * Construct the normalization utility.  Default to normalization range of -1 to +1.
	 * @param dataHigh The high value for the input data.
	 * @param dataLow The low value for the input data.
	 */
	public NormUtil(double dataHigh, double dataLow) {
		this(dataHigh,dataLow,1,-1);
	}
 
	/**
	 * Construct the normalization utility, allow the normalization range to be specified.
	 * @param dataHigh The high value for the input data.
	 * @param dataLow The low value for the input data.
	 * @param normalizedHigh The high value for the normalized data.
	 * @param normalizedLow The low value for the normalized data. 
	 */
	public NormUtil(double dataHigh, double dataLow, double normalizedHigh, double normalizedLow) {
            init(dataHigh, dataLow, normalizedHigh, normalizedLow);
	}
        
        public void init(double dataHigh, double dataLow, double normalizedHigh, double normalizedLow) {
		this.dataHigh = dataHigh;
		this.dataLow = dataLow;
		this.normalizedHigh = normalizedHigh;
		this.normalizedLow = normalizedLow;
                log.fine(dataLow + ":" + dataHigh + " -> " + normalizedLow + ":" + normalizedHigh);            
        }

        
	/**
	 * Construct the normalization utility, allow the normalization range to be specified.
	 * @param dataHigh The high value for the input data.
	 * @param dataLow The low value for the input data.
	 * @param normalizedHigh The high value for the normalized data.
	 * @param normalizedLow The low value for the normalized data. 
	 */
	public NormUtil(Object dataHigh, Object dataLow, double normalizedHigh, double normalizedLow) {
            // cast            
            init(toDouble(dataHigh), toDouble(dataLow), normalizedHigh, normalizedLow);
        }
        

        public Double toDouble(Object obj){
            return new Double(obj.toString());
        }
        
        
 
	/**
	 * Normalize x.
	 * @param x The value to be normalized.
	 * @return The result of the normalization.
	 */
	public double normalize(double x) {
		return ((x - dataLow) 
				/ (dataHigh - dataLow))
				* (normalizedHigh - normalizedLow) + normalizedLow;
	}
        
	/**
	 * Normalize x.
	 * @param x The value to be normalized.
	 * @return The result of the normalization.
	 */
	public double normalize(Object x) {
            return normalize(toDouble(x).doubleValue());
        }        
 
	/**
	 * Denormalize x.  
	 * @param x The value to denormalize.
	 * @return The denormalized value.
	 */
	public double denormalize(double x) {
		return ((dataLow - dataHigh) * x - normalizedHigh
				* dataLow + dataHigh * normalizedLow)
				/ (normalizedLow - normalizedHigh);
	}
 
	public static void main(String[] args) {
		NormUtil norm = new NormUtil(1,10);
 
		double start = 7;
 
		double x = norm.normalize(start);
		System.out.println(start + " normalized is " + x);
		System.out.println(x + " denormalized is " + norm.denormalize(x) );
	}
}