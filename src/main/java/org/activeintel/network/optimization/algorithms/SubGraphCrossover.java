/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.beans.*;
import java.io.Serializable;

import java.beans.*;
import java.io.Serializable;

import java.util.List;
import java.util.Random;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Vector;
import org.jgrapht.Graph;
import org.jgrapht.graph.Subgraph;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.ContinuousUniformGenerator;
import org.uncommons.maths.random.DiscreteUniformGenerator;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.operators.AbstractCrossover;
import org.uncommons.watchmaker.framework.operators.ObjectArrayCrossover;

/**
 * Delegates crossover (with graph-oriented modifications) to {@code org.uncommons.watchmaker.framework.operators.ObjectArrayCrossover}
 * 
 * TODO LATER: Use Generics
 * 
 * @author Neil Rubens
 */public class SubGraphCrossover extends AbstractCrossover<Graph> implements Serializable, EvolutionaryOperator<Graph> {
    
    
    ObjectArrayCrossover<Object> delegate;
    private Graph graph;
    private Integer crossoverPoints;
    
    
    
    public SubGraphCrossover() {
        super(1);
    }
    
    
    /**
     * Sets up a cross-over implementation that uses a variable number of cross-over
     * points.  Cross-over is applied to a proportion of selected parent pairs, with
     * the remainder copied unchanged into the output population.  The size of this
     * evolved proportion is controlled by the {@code crossoverProbabilityVariable}
     * parameter.
     * @param crossoverPointsVariable A variable that provides a (possibly constant,
     * possibly random) number of cross-over points for each cross-over operation.
     * @param crossoverProbabilityVariable A variable that controls the probability
     * that, once selected, a pair of parents will be subjected to cross-over rather
     * than being copied, unchanged, into the output population.
     */    
    public SubGraphCrossover(Graph graph, NumberGenerator<Integer> crossoverPointsVariable)                                
    {
        super(crossoverPointsVariable);        
        setCrossoverPointsVariable(crossoverPointsVariable);
        setGraph(graph);
    }

    
    public void setCrossoverPointsVariable(NumberGenerator<Integer> crossoverPointsVariable){
        delegate = new ObjectArrayCrossover<Object>(crossoverPointsVariable);               
    }

    
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Graph> mate(Graph parent1, Graph parent2, int i, Random random) {
        Object[] parent1_nodes = parent1.vertexSet().toArray();
        Object[] parent2_nodes = parent2.vertexSet().toArray();
        List<Object[]> parents = new ArrayList<Object[]>();
        parents.add(parent1_nodes);
        parents.add(parent2_nodes); 
        
        // Crossover Node Arrays
        List<Object[]> childrenNodeSet = delegate.apply(parents, random);

        // Fixup so that graph properties are preserved
        List<Graph> children = new ArrayList<Graph>();
        for (Object[] child_nodes: childrenNodeSet){
            Graph subGraph = adjustGraph(parent1, parent2, child_nodes, parent1_nodes, parent2_nodes, random);            
            children.add(subGraph);
        }                

        return children;        
    }

    /**
     * Fix up so that graph properties are preserved:
     * no duplicates
     * fixed size
     * 
     * If there are not enough nodes in the subGraph (made out of child_nodes)
     * Add at random nodes from parents.
     * 
     * @param child_nodes
     * @param parent1_nodes
     * @param parent2_nodes
     * @return 
     */
    private Graph adjustGraph(Graph g1, Graph g2, Object[] child_nodes, Object[] parent1_nodes, Object[] parent2_nodes, Random random) {
        // TODO
        // use graph to create subgraphs
        // Use Set to get rid of duplicates
        HashSet<Object> subGraphNodeSet = new HashSet<Object>(Arrays.asList(child_nodes));
        HashSet<Object> parentNodeSet = new HashSet<Object>(Arrays.asList(parent1_nodes));        
        parentNodeSet.addAll(Arrays.asList(parent2_nodes));
        // Use vector for get operations
        ArrayList<Object> parentNodeList = new ArrayList<Object>(parentNodeSet);
        
        // If there are not enough nodes in the subGraph (made out of child_nodes)
        // add at random nodes from parents
        while (subGraphNodeSet.size() < parent1_nodes.length){
            Object node = parentNodeList.get(random.nextInt(parent1_nodes.length));
            subGraphNodeSet.add(node);
        }
        
        // Create the subgraph
        Subgraph subGraph = (Subgraph) SubGraphFactory.createSubgraph(this.getGraph(), g1, subGraphNodeSet, graph.edgeSet()); //hack: including all of the edges (hopefully the edges with no nodes in subgraph will be filtered out)
        
        return subGraph;
    }

    /**
     * @return the crossoverPoints
     */
    public Integer getCrossoverPoints() {
        return crossoverPoints;
    }

    /**
     * @param crossoverPoints the crossoverPoints to set
     */
    public void setCrossoverPoints(Integer crossoverPoints) {
        this.crossoverPoints = crossoverPoints;
        delegate = new ObjectArrayCrossover<Object>(crossoverPoints);               
    }

    /**
     * @return the graph
     */
    public Graph getGraph() {
        return graph;
    }

    /**
     * @param graph the graph to set
     */
    public void setGraph(Graph graph) {
        this.graph = graph;
    }
    
    
       

}
