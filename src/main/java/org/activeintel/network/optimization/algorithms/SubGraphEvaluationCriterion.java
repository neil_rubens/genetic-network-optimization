/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.beans.*;
import java.io.Serializable;
import java.util.logging.Logger;

/**
 *
 * @author neil
 */
public class SubGraphEvaluationCriterion implements Serializable {

    private static final Logger log = Logger.getLogger(SubGraphEvaluationCriterion.class.getName());                
    
    public static final String PROP_SAMPLE_PROPERTY = "sampleProperty";
    public static final String PROP_WEIGHT_PROPERTY = "weight";        
    public static final String PROP_METRIC_PROPERTY = "metricName";    
    private String sampleProperty = "test";
    private String metricName = "metricName";    
    private Boolean isNormalized = Boolean.TRUE;
    private PropertyChangeSupport propertySupport;
    private Float weight = new Float(0.0);
    
    public String toString(){
        StringBuffer strb = new StringBuffer();
        strb.append("name: " + metricName );
        strb.append(", ");
        strb.append("weight: " + weight );        
        return strb.toString();
    }
    
    public SubGraphEvaluationCriterion() {
        init();
    }
    
    public void init(){
        propertySupport = new PropertyChangeSupport(this);                        
    }

    SubGraphEvaluationCriterion(String name) {
        init();
        setName(name);
    }
    
    
    public String getSampleProperty() {
        return sampleProperty;
    }
    
    public void setSampleProperty(String value) {
        String oldValue = sampleProperty;
        sampleProperty = value;
        propertySupport.firePropertyChange(PROP_SAMPLE_PROPERTY, oldValue, sampleProperty);
    }

    public String getMetricName() {
        return metricName;
    }
    
    public void setMetricName(String value) {
        String oldValue = metricName;
        metricName = value;
        propertySupport.firePropertyChange(PROP_METRIC_PROPERTY, oldValue, metricName);
    }
    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertySupport.removePropertyChangeListener(listener);
    }

    /**
     * @return the name
     */
    public String getName() {
        return metricName;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {        
        this.metricName = name;
        log.finer("name: " + this.metricName);
        propertySupport.firePropertyChange(PROP_METRIC_PROPERTY, metricName, metricName);        
    }

    /**
     * @return the weight
     */
    public Float getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Float weight) {
        log.finest("weight: " + this.weight + " -> " + weight);
        this.weight = weight;
    }

    /**
     * @return the isNormalized
     */
    public Boolean getIsNormalized() {
        return isNormalized;
    }

    /**
     * @param isNormalized the isNormalized to set
     */
    public void setIsNormalized(Boolean isNormalized) {
        this.isNormalized = isNormalized;
    }
}
