/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms.ui;

import org.jdesktop.beansbinding.Converter;

/**
 *
 * @author neil
 */
public class StringToIntegerConverter  extends Converter{

    @Override
    public Object convertForward(Object obj) {
        Integer intObj = new Integer(-1);
        //System.out.println("convertForward"); 
        try{
            intObj = Integer.valueOf((String) obj);            
        } catch (java.lang.NumberFormatException ex){
            
        }
        
        return intObj;
    }

    @Override
    public Object convertReverse(Object obj) {
        //System.out.println("convertReverse: " + obj);        
        return obj.toString();
    }

    
}
