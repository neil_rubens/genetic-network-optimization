/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.beans.*;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import org.jgrapht.Graph;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.DiscreteUniformGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvaluatedCandidate;
import org.uncommons.watchmaker.framework.EvolutionEngine;
import org.uncommons.watchmaker.framework.EvolutionObserver;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.GenerationalEvolutionEngine;
import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.TerminationCondition;
import org.uncommons.watchmaker.framework.termination.GenerationCount;


/**
 * This class attempts to make EvolutionEngine engine configurable
 * through the use of beans; by using beans and delegate patterns.
 * 
 * Original methods are accessible through parameterized calls
 * which will be delegated.
 * 
 * If beans configuration is used; evolve() should be called
 * at which point parameters (that have been set) will be used
 * 
 * ENHANCEMENT: might provide hybrid option of using 
 * both parameters and some of the preset variables
 * 
 * @author Neil Rubens
 */
public class GeneticOptimizer implements Serializable, EvolutionEngine<Graph> {    
    
    private PropertyChangeSupport propertyChangeSupport; // handles observalbe pattern (beans)
            
    
    EvolutionEngine<Graph> engine;


    private Integer populationSize;
    private Integer eliteCount; // use either eliteCount or eliteRatio
    private Float eliteRatio; // use either eliteCount or eliteRatio   
    private Integer stagnationGenerationLimit;
    private LinkedList<EvolutionaryOperator<Graph>> evolutionaryOperators;  
    private String status;
    public static final String PROP_STATUS_PROPERTY = "status";    
    public static final String STATUS_RUNNING = "running";
    public static final String STATUS_FINISHED = "finished";    
    
    private List<EvolutionObserver<? super Graph>> evolutionObservers;
    
    
/*    
    public static final String PROP_POPULATION_SIZE = "populationSize";        
    public static final String PROP_ELITE_COUNT = "eliteCount";        
    public static final String PROP_ELITE_RATIO = "eliteRatio";        
    public static final String PROP_STAGNATION_GENERATION_LIMIT = "stagnationGenerationLimit";
*/
    private ArrayList<TerminationCondition> terminationConditions;
    private CandidateFactory<Graph> candidateFactory;
    private FitnessEvaluator<Graph> fitnessEvaluator;
    private Graph optimizedGraph; // result of optimization
       
    
    
    public GeneticOptimizer() {
        init();
    }
    
    public void init(){
        evolutionaryOperators = new LinkedList<EvolutionaryOperator<Graph>>();        
        evolutionObservers = new ArrayList<EvolutionObserver<? super Graph>>();
        terminationConditions = new ArrayList<TerminationCondition>();
        propertyChangeSupport = new PropertyChangeSupport(this);        
    }
    

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }
    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    
    public void addEvolutionaryOperator(EvolutionaryOperator<Graph> operator){
        evolutionaryOperators.add(operator);
    }
    
    
    public void setCandidateFactory(CandidateFactory<Graph> candidateFactory){
        this.candidateFactory = candidateFactory;
    }
    
    
    public void setFitnessEvaluator(FitnessEvaluator<Graph> eval){
        this.fitnessEvaluator = eval;
    }
    
    
    /**
     * Creates GeneticOptimizer w/ some of the values initialized
     * used mostly for testing
     * 
     * @return 
     */
    public static GeneticOptimizer createGeneticOptimizer(Graph graph){
        
        GeneticOptimizer optimizer = new GeneticOptimizer();
        optimizer.setPopulationSize(100);
        optimizer.setEliteCount(5);
        
        optimizer.setCandidateFactory(SubGraphFactory.createSubGraphFactory(graph));        
        optimizer.addEvolutionaryOperator(SubGraphMutation.createSubGraphMutation(graph));
                        
        SubGraphEvaluator evaluator = new SubGraphEvaluator();
        evaluator.setSuperGraph(graph);        
        optimizer.setFitnessEvaluator(evaluator);

        optimizer.addTerminationCondition(new GenerationCount(100));
        
        
        return optimizer;
    }
    
    
    
    
    public void addTerminationCondition(TerminationCondition cond){
        terminationConditions.add(cond);
    }
    

    /**
     * ReFactor out; has a lot of hardcoded parts
     * 
     * @param graph
     * @param numSubGraphNodes
     * @param pMutation
     * @param populationSize
     * @param eliteCount 
     */
    public void configure(Graph graph, int numSubGraphNodes, Probability pMutation, int populationSize, int eliteCount){
        // TODO: remove; replace w/ constructor
        CandidateFactory<Graph> factory = new SubGraphFactory(graph, numSubGraphNodes);        
        
        // Create a pipeline that applies cross-over then mutation.
        LinkedList<EvolutionaryOperator<Graph>> operators = new LinkedList<EvolutionaryOperator<Graph>>();
        operators.add( new SubGraphMutation(graph, new Float(0.05)) );
        NumberGenerator<Integer> crossoverPointsVariable = new DiscreteUniformGenerator(1, numSubGraphNodes, new MersenneTwisterRNG()); 
        operators.add( new SubGraphCrossover(graph, crossoverPointsVariable) );        
        EvolutionaryOperator<Graph> pipeline = new EvolutionPipeline<Graph>(operators);

        final FitnessEvaluator<Graph> fitnessEvaluator = (FitnessEvaluator<Graph>) new SubGraphEvaluator();        
        
        SelectionStrategy<Object> selection = new RouletteWheelSelection();
        Random rng = new MersenneTwisterRNG();        

        engine = new GenerationalEvolutionEngine<Graph>(factory,
                                                      pipeline,
                                                      fitnessEvaluator,
                                                      selection,
                                                      rng);

        engine.addEvolutionObserver(new EvolutionObserver<Graph>()
        {
            public void populationUpdate(PopulationData<? extends Graph> data)
            {
                System.out.printf("Generation %d: %f\n",
                                  data.getGenerationNumber(),
                                  fitnessEvaluator.getFitness(data.getBestCandidate(), null));
            }
        });        
        
        
    }

    


    /**
     * ReFactor out; Don't use
     * 
     * @param graph
     * @param numSubGraphNodes
     * @param pMutation
     * @param populationSize
     * @param eliteCount
     * @param terminationCondition
     * @return 
     */
    public Graph optimize(Graph graph, int numSubGraphNodes, Probability pMutation, int populationSize, int eliteCount, TerminationCondition terminationCondition){
        configure(graph, numSubGraphNodes, pMutation, populationSize, eliteCount);
        
        Graph result = engine.evolve(populationSize, eliteCount, terminationCondition);        
                
        return result;
    }

    
    
    /**
     * @return the populationSize
     */
    public Integer getPopulationSize() {
        return populationSize;
    }

    /**
     * @param populationSize the populationSize to set
     */
    public void setPopulationSize(Integer populationSize) {
        this.populationSize = populationSize;
    }

    /**
     * @return the eliteCount
     */
    public Integer getEliteCount() {
        if (eliteCount == null){
            return Math.round(this.populationSize * this.eliteRatio);
        }
        else {
            return eliteCount;        
        }
    }

    /**
     * @param eliteCount the eliteCount to set
     */
    public void setEliteCount(Integer eliteCount) {
        this.eliteCount = eliteCount;
    }

    /**
     * @return the eliteRatio
     */
    public Float getEliteRatio() {
        return eliteRatio;
    }

    /**
     * @param eliteRatio the eliteRatio to set
     */
    public void setEliteRatio(Float eliteRatio) {
        this.eliteRatio = eliteRatio;
        // since using ratio need to update count
        if (populationSize != null && eliteRatio != null){
            this.eliteCount = Math.round(this.populationSize * this.eliteRatio);
        }
    }

    /**
     * @return the stagnationGenerationLimit
     */
    public Integer getStagnationGenerationLimit() {
        return stagnationGenerationLimit;
    }

    /**
     * @param stagnationGenerationLimit the stagnationGenerationLimit to set
     */
    public void setStagnationGenerationLimit(Integer stagnationGenerationLimit) {
        this.stagnationGenerationLimit = stagnationGenerationLimit;
    }
    
    
    /**
     * Creates engine based on the class variables
     * and runs evolve on it
     * 
     * @return 
     */
    public Graph evolve() {
        // Create Engine               
        this.engine = new GenerationalEvolutionEngine( candidateFactory, new EvolutionPipeline<Graph>(this.evolutionaryOperators), getFitnessEvaluator(), new RouletteWheelSelection(), new MersenneTwisterRNG());        
        
        // Add observers
        for (EvolutionObserver observer : this.evolutionObservers){
            this.engine.addEvolutionObserver(observer);
        }
        
        // Launch engine (& notify listeners)
        this.setStatus( STATUS_RUNNING );
        Graph returnObj = this.engine.evolve( this.populationSize, this.getEliteCount(), terminationConditions.toArray(new TerminationCondition[terminationConditions.size()]));
        this.setStatus( STATUS_FINISHED );
        
        setOptimizedGraph(returnObj);
        
        return returnObj;
    }    

    
    @Override
    public Graph evolve(int i, int i1, TerminationCondition... tcs) {
        return this.engine.evolve(i, i1, tcs);
    }

    @Override
    public Graph evolve(int i, int i1, Collection<Graph> clctn, TerminationCondition... tcs) {
        return this.engine.evolve(i, i1, clctn, tcs);
    }

    @Override
    public List<EvaluatedCandidate<Graph>> evolvePopulation(int i, int i1, TerminationCondition... tcs) {
        return this.engine.evolvePopulation(i, i1, tcs);
    }

    @Override
    public List<EvaluatedCandidate<Graph>> evolvePopulation(int i, int i1, Collection<Graph> clctn, TerminationCondition... tcs) {
        return this.engine.evolvePopulation(i, i1, clctn, tcs);
    }

    @Override
    public void addEvolutionObserver(EvolutionObserver<? super Graph> eo) {
        this.evolutionObservers.add(eo);
    }

    @Override
    public void removeEvolutionObserver(EvolutionObserver<? super Graph> eo) {
        this.evolutionObservers.remove(eo);
    }

    @Override
    public List<TerminationCondition> getSatisfiedTerminationConditions() {
        return this.engine.getSatisfiedTerminationConditions();
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        propertyChangeSupport.firePropertyChange(PROP_STATUS_PROPERTY, this.status, status);                            
        this.status = status;
    }

    private void setOptimizedGraph(Graph graph) {
        this.optimizedGraph = graph;
    }
    
    public Graph getOptimizedGraph() {
        return this.optimizedGraph;
    }

    /**
     * @return the fitnessEvaluator
     */
    public FitnessEvaluator<Graph> getFitnessEvaluator() {
        return fitnessEvaluator;
    }

        

    
}
