/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms.ui;

import org.jdesktop.beansbinding.Converter;

/**
 *
 * @author neil
 */
public class StringToFloatConverter  extends Converter{

    @Override
    public Object convertForward(Object obj) {
        Float convObj = new Float(0);
        //System.out.println("convertForward"); 
        try{
            convObj = Float.valueOf((String) obj);            
        } catch (java.lang.NumberFormatException ex){
            System.out.println("non-lethal exception; will keep running:");
            ex.printStackTrace();
        }
        
        return convObj;
    }

    @Override
    public Object convertReverse(Object obj) {
        //System.out.println("convertReverse: " + obj);        
        return obj.toString();
    }

    
}
