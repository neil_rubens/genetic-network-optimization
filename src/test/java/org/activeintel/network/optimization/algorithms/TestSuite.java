/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author neil
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({org.activeintel.network.optimization.algorithms.GeneticOptimizerTest.class, org.activeintel.network.optimization.algorithms.SubGraphCrossoverTest.class, org.activeintel.network.optimization.algorithms.SubGraphMutationTest.class, org.activeintel.network.optimization.algorithms.SubGraphEvaluatorTest.class, org.activeintel.network.optimization.algorithms.SubGraphFactoryTest.class})
public class TestSuite {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
