/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import org.jgrapht.Graph;
import org.jgrapht.graph.Subgraph;

import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import java.io.File;
import java.net.URISyntaxException;
import java.util.Random;
import junit.framework.Assert;
import org.activeintel.gephi.jgrapht.JGraphTUtils;
import org.activeintel.gephi.utilities.GephiUtilities;
import org.junit.Test;


/**
 *
 * @author neil
 */
public class SubGraphEvaluatorTest {
    
    /**
     * 
     */    
    @Test
    public void testGenerateRandomCandidateThroughput() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);        
        Random rnd = new Random();
        
        SubGraphFactory factory = new SubGraphFactory(graph, 30);
        SubGraphEvaluator evaluator = new SubGraphEvaluator();
        evaluator.setSuperGraph(graph);
        
        // Load Testing
        for (int i = 0; i < 1000; i++ ){
            Graph candidate = factory.generateRandomCandidate(rnd);
            double fitness = evaluator.getFitness(candidate, null);
            System.out.println(fitness);
        }        
    }

    
    /**
     * For some reason this test fails when run alongside with other tests
     * but works individually; might be caused by gephi
     * 
     * @throws URISyntaxException
     * @throws FileNotFoundException 
     */
    @Test
    public void getFitness() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("2012.04.20-v1.1.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);        
        Random rnd = new Random();
        
        SubGraphFactory factory = new SubGraphFactory(graph, 30);
        SubGraphEvaluator evaluator = new SubGraphEvaluator();
        evaluator.setSuperGraph(graph);
        evaluator.setIsNormalized(true);        
        evaluator.setGraphInDegreeUtility((float)0);
        evaluator.setGraphOutDegreeUtility((float)0);
        evaluator.setNodeUtility((float)-1);
        evaluator.setSubGraphInDegreeUtility((float)0.2);
        evaluator.setSubGraphOutDegreeUtility((float)-0.05);
                
        
        // Create certain subgraphs (good and bad); and make sure that fitness(good) > fitness(bad)
        Subgraph badSubgraph = factory.createSubgraph(new Integer[] {20,45,13,8,9,35,47,11,27,7});
        // node 9; seems usesless (disconnected from the subgraph) replace it with node 31 which has in and out edge from subgraph
        // resulting subgraph should have a higher fitness score
        Subgraph goodSubgraph = factory.createSubgraph(new Integer[] {20,45,13,8,31,35,47,11,27,7});    
        
        System.out.println(badSubgraph);
        System.out.println(goodSubgraph);        
        
        System.out.println(evaluator.getFitness(badSubgraph, null));
        System.out.println(evaluator.getFitness(goodSubgraph, null));        
        
        Assert.assertTrue(evaluator.getFitness(goodSubgraph, null) > evaluator.getFitness(badSubgraph, null));
        
        // test w/o normalization
        evaluator.setIsNormalized(false);                
        System.out.println(evaluator.getFitness(badSubgraph, null));
        System.out.println(evaluator.getFitness(goodSubgraph, null));        
        
        Assert.assertTrue(evaluator.getFitness(goodSubgraph, null) > evaluator.getFitness(badSubgraph, null));        
        
    }    
    
    
    
}
