/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;


import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import java.io.File;
import java.net.URISyntaxException;
import java.util.Random;
import junit.framework.Assert;
import org.activeintel.gephi.jgrapht.JGraphTUtils;
import org.activeintel.gephi.utilities.GephiUtilities;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.jgrapht.Graph;
import org.activeintel.network.optimization.algorithms.SubGraphFactory;
import org.uncommons.maths.random.MersenneTwisterRNG;





/**
 *
 * @author neil
 */
public class SubGraphFactoryTest {
    
    
    /**
     * Make sure that the graph on which tests are executed gets loaded properly
     */
    @Test
    public void testImportFile() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);        
    }
    
    
    @Test
    public void testGenerateRandomCandidate() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        org.gephi.graph.api.Graph graph = graphModel.getGraph();
        
        // record specs of original graph
        int graphNodeNum = graph.getNodeCount();
        int graphEdgeNum = graph.getEdgeCount();
        
        
        int subGraphNodeNum = 30;        
        org.jgrapht.Graph graphT = JGraphTUtils.wrap(graphModel);        
        SubGraphFactory factory = new SubGraphFactory(graphT, subGraphNodeNum);
        org.jgrapht.Graph subGraph = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
                
        // Test that the number of nodes is as expected
        Assert.assertEquals(subGraphNodeNum, subGraph.vertexSet().size());

        // Test num of edges
        Assert.assertTrue(subGraph.edgeSet().size() > 0);
        
        // Test that the original graph is unchanged
        Assert.assertEquals(graphNodeNum, graph.getNodeCount());
        Assert.assertEquals(graphEdgeNum, graph.getEdgeCount());        
    }

    
    /**
     * 
     */
    @Test
    public void testGenerateRandomCandidateThroughput() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        org.jgrapht.Graph graph = JGraphTUtils.wrap(graphModel);                
        Random rnd = new Random();
        
        SubGraphFactory factory = new SubGraphFactory(graph, 30);
        
        // Load Testing
        for (int i = 0; i < 10000; i++ ){
            System.out.println(i);
            factory.generateRandomCandidate(rnd);
        }
    }

    
    /**
     * 
     */
    @Test
    public void testSubgraphSize() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        org.jgrapht.Graph graph = JGraphTUtils.wrap(graphModel);                
        Random rnd = new Random();
        
        int numNodes = 30;
        SubGraphFactory factory = new SubGraphFactory(graph, numNodes);
        
        // Load Testing
        for (int i = 0; i < 10000; i++ ){
            System.out.println(i);
            org.jgrapht.Graph candidate = factory.generateRandomCandidate(rnd);
            Assert.assertEquals(numNodes, candidate.vertexSet().size());
            Assert.assertTrue( candidate.edgeSet().size() > 0 ); // should have edges
        }
    }
    
     
    
}
