/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import org.activeintel.gephi.jgrapht.JGraphTUtils;
import java.util.ArrayList;
import java.util.Iterator;
import org.uncommons.maths.random.Probability;
import org.uncommons.maths.random.ContinuousUniformGenerator;
import org.uncommons.maths.random.DiscreteUniformGenerator;
import java.lang.Integer;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;
import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import java.net.URISyntaxException;
import org.activeintel.gephi.utilities.GephiUtilities;
import java.io.File;
import java.util.List;
import java.util.Random;
import junit.framework.Assert;
import org.jgrapht.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.core.IsEqual.*;
import static org.hamcrest.core.IsNot.*;


/**
 *
 * @author neil
 */
public class SubGraphMutationTest {
    
    /**
     * 
     */
    @Test
    public void testMutate() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);
        
        int subGraphNodeNum = 10;        
        SubGraphFactory factory = new SubGraphFactory(graph, subGraphNodeNum);
        
        org.jgrapht.Graph parentGraph1 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
        org.jgrapht.Graph parentGraph2 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
        List<Graph> candidates = new ArrayList<Graph>();
        candidates.add(parentGraph1);
        candidates.add(parentGraph2);   
        
        SubGraphMutation mutator = new SubGraphMutation(graph, (float) 0.3); // high probability to ensure that mutation will occur
        List<Graph> mutants = mutator.apply(candidates, new Random());
        
        
        //Assert.assertArrayEquals();
        // nodes should differ
        assertThat(mutants.get(0).vertexSet().toArray(), not(equalTo(parentGraph1.vertexSet().toArray())));
        assertThat(mutants.get(1).vertexSet().toArray(), not(equalTo(parentGraph2.vertexSet().toArray())));        
        
        System.out.println("parents: ");
        System.out.println(parentGraph1); 
        System.out.println(parentGraph2);         
        System.out.println("mutants: ");        
        System.out.println(mutants);
    }

    
    /**
     * 
     */
    @Test
    public void testMassMutate() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);
        
        int subGraphNodeNum = 20;        
        SubGraphFactory factory = new SubGraphFactory(graph, subGraphNodeNum);
        
        for (int i = 0; i < 1000; i++){
            System.out.println(i);
            org.jgrapht.Graph parentGraph1 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
            org.jgrapht.Graph parentGraph2 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
            List<Graph> candidates = new ArrayList<Graph>();
            candidates.add(parentGraph1);
            candidates.add(parentGraph2);   

            SubGraphMutation mutator = new SubGraphMutation(graph, (float) 0.3); // high probability to ensure that mutation will occur
            List<Graph> mutants = mutator.apply(candidates, new Random());
            Iterator<Graph> itr = mutants.iterator();
            
            while (itr.hasNext()){
                Graph gMutants = itr.next();
                Assert.assertEquals(subGraphNodeNum, gMutants.vertexSet().size());
                Assert.assertTrue(gMutants.edgeSet().size() > 0); // should have some edges
            }            
        }
    }
    
    
}
