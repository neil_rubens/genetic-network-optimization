/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import org.jgrapht.Graph;
import static org.junit.Assert.*;
import static org.hamcrest.core.IsEqual.*;
import static org.hamcrest.core.IsNot.*;

import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import java.io.File;
import java.net.URISyntaxException;
import java.util.Random;
import junit.framework.Assert;
import org.activeintel.gephi.jgrapht.JGraphTUtils;
import org.activeintel.gephi.utilities.GephiUtilities;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.uncommons.maths.random.MersenneTwisterRNG;
import org.uncommons.maths.random.Probability;
import org.uncommons.watchmaker.framework.termination.GenerationCount;


/**
 *
 * @author neil
 */
public class GeneticOptimizerTest {
    
    
    /**
     * 
     */
    @Test
    public void testEvolve() throws URISyntaxException, FileNotFoundException {
        System.out.println(this.getClass().getResource("."));
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());

        GraphModel graphModel = GephiUtilities.importFile(file); 
        Graph graph = JGraphTUtils.wrap(graphModel);                
        
        GeneticOptimizer optimizer = GeneticOptimizer.createGeneticOptimizer(graph);
        optimizer.setPopulationSize(10);
                
        SubGraphEvaluator evaluator = new SubGraphEvaluator();
        evaluator.setSuperGraph(graph);
        optimizer.setFitnessEvaluator(evaluator);
        
        //Graph result = optimizer.optimize(graph, 20, new Probability(0.05), 10, 1, new GenerationCount(1000));
        Graph result = optimizer.evolve();
        
        Assert.assertTrue( result.vertexSet().size() > 0 ); // should be more than zero nodes
        Assert.assertTrue( result.edgeSet().size() > 0 ); // should be more than zero edges
        
        System.out.println(result);
        System.out.println("fitness: " + evaluator.getFitness(result));     
        
        
        
    }
    
    
}
