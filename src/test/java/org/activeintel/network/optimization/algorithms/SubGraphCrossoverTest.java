/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.activeintel.network.optimization.algorithms;

import java.util.HashSet;
import java.util.Set;
import org.jgrapht.graph.Subgraph;
import org.activeintel.gephi.jgrapht.JGraphTUtils;
import java.util.Iterator;
import org.uncommons.maths.random.Probability;
import org.uncommons.maths.random.ContinuousUniformGenerator;
import org.uncommons.maths.random.DiscreteUniformGenerator;
import java.lang.Integer;
import org.uncommons.maths.number.NumberGenerator;
import org.uncommons.maths.random.MersenneTwisterRNG;
import java.io.FileNotFoundException;
import org.gephi.graph.api.GraphModel;
import java.net.URISyntaxException;
import org.activeintel.gephi.utilities.GephiUtilities;
import java.io.File;
import java.util.List;
import java.util.Random;
import org.jgrapht.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author neil
 */
public class SubGraphCrossoverTest {
    
    public SubGraphCrossoverTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of mate method, of class SubGraphCrossover.
     */
    @Test
    public void testMate() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);
        
        int subGraphNodeNum = 10;        
        SubGraphFactory factory = new SubGraphFactory(graph, subGraphNodeNum);
        
        org.jgrapht.Graph parentGraph1 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
        org.jgrapht.Graph parentGraph2 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
        
        NumberGenerator<Integer> crossoverPointsVariable = new DiscreteUniformGenerator(1, subGraphNodeNum, new Random());
        SubGraphCrossover crossover = new SubGraphCrossover(graph, crossoverPointsVariable);
        
        List<Graph> children = crossover.mate(parentGraph1, parentGraph2, 1, new Random());
        System.out.println("parents: ");
        System.out.println(parentGraph1); 
        System.out.println(parentGraph2);         
        System.out.println("children: ");        
        System.out.println(children);
    }
    
    
    /**
     * Test of mate method, of class SubGraphCrossover.
     */
    @Test
    public void testAdjustGraph() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);
        
        int subGraphNodeNum = 20;        
        SubGraphFactory factory = new SubGraphFactory(graph, subGraphNodeNum);
        
        org.jgrapht.Graph parentGraph1 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
        org.jgrapht.Graph parentGraph2 = (org.jgrapht.Graph) factory.generateRandomCandidate(new Random());
                
        
        // arguments of createSubgraph method are somewhat hacky: using all edges from supergraph
        Set edges = new HashSet( parentGraph1.edgeSet() );
        edges.addAll(parentGraph2.edgeSet());        
        Subgraph subGraph = (Subgraph) SubGraphFactory.createSubgraph(parentGraph1, parentGraph1, parentGraph1.vertexSet(), edges);        
        
        Assert.assertEquals(parentGraph1.edgeSet().size(), subGraph.edgeSet().size());
        
        
    }
    
    
    /**
     * Test of mate method, of class SubGraphCrossover.
     */
    @Test
    public void testMassMate() throws URISyntaxException, FileNotFoundException {
        File file = new File(getClass().getClassLoader().getResource("les-miserables.gexf").getFile());
        GraphModel graphModel = GephiUtilities.importFile(file);  
        Graph graph = JGraphTUtils.wrap(graphModel);
        
        int subGraphNodeNum = 20;        
        SubGraphFactory factory = new SubGraphFactory(graph, subGraphNodeNum);
        
        for (int i = 0; i < 10000; i++){        
            System.out.println(i);
            org.jgrapht.Graph parentGraph1 = factory.generateRandomCandidate(new Random());
            org.jgrapht.Graph parentGraph2 = factory.generateRandomCandidate(new Random());

            System.out.print("t1");
            NumberGenerator<Integer> crossoverPointsVariable = new DiscreteUniformGenerator(1, subGraphNodeNum, new Random());
            SubGraphCrossover crossover = new SubGraphCrossover(graph, crossoverPointsVariable);
            System.out.print("t2");
            List<Graph> children = crossover.mate(parentGraph1, parentGraph2, 1, new Random());
            System.out.print("t2.5");
            Iterator<Graph> itr = children.iterator();
            System.out.print("t3");
            while (itr.hasNext()){
                System.out.print(".");
                Graph gChild = itr.next();
                Assert.assertEquals(subGraphNodeNum, gChild.vertexSet().size());
            }
        }
    }
    
    
}
